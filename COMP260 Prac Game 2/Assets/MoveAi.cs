﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]

public class MoveAi : MonoBehaviour {
	public Transform puck;
	public Transform gate;
	private Rigidbody rigidbody;


	// Use this for initialization

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	void onDrawGizmos(){
		/*
		Gizmos.color = Color.yellow;
		Vector3 pos = GetMousePosition();
		Gizmos.DrawLine(Camera.main.transform.position, pos);
		*/
	}

	public float speed = 20f;

	void FixedUpdate () {

		Vector3 direction = Vector3.zero;
	
		direction = puck.position - transform.position;

		rigidbody.velocity = direction.normalized * speed;


	}

	/*
	void FixedUpdate () {
		Vector3 pos = direction();
		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	}
	*/


	// Update is called once per frame
	void Update () {

	}
}
